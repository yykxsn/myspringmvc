package com.tczs.springmvc;

import java.util.ArrayList;
import java.util.List;

public class TestData {

    public static List<Person> personList = new ArrayList<>();

    static {
        personList.add(new Person(1,"litao",23,"2332423","156854554"));
        personList.add(new Person(2,"litao",25,"23453415","243256"));
        personList.add(new Person(3,"xiaohua",25,"1235345","43543654"));
        personList.add(new Person(4,"xiaohu",34,"3254354364","34535545"));
    }

    static class Person{
        private int id;
        private String name;
        private int age;
        private String idNo;
        private String mobile;

        public Person(int id, String name, int age, String idNo, String mobile) {
            this.id = id;
            this.name = name;
            this.age = age;
            this.idNo = idNo;
            this.mobile = mobile;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public int getAge() {
            return age;
        }

        public void setAge(int age) {
            this.age = age;
        }

        public String getIdNo() {
            return idNo;
        }

        public void setIdNo(String idNo) {
            this.idNo = idNo;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }
    }
}
