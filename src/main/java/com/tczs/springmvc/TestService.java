package com.tczs.springmvc;

import com.tczs.ioc.annotation.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class TestService {

    public TestData.Person findById(int id){
        for(TestData.Person person : TestData.personList){
            if(person.getId() == id){
                return person;
            }
        }
        return null;
    }

    public List<TestData.Person> findByParam(Integer id, String name, String mobile){
        List<TestData.Person> peopleList = new ArrayList<>();
        for(TestData.Person person : TestData.personList){
            boolean idFlag = id == null ? true:person.getId()==id;
            boolean nameFlag = name == null ? true:person.getName().equals(name);
            boolean mobileFlag = mobile == null ? true:person.getMobile().equals(mobile);
            if(idFlag && nameFlag && mobileFlag){
                peopleList.add(person);
            }
        }
        return  peopleList;
    }
}
