package com.tczs.springmvc;

import com.tczs.ioc.annotation.BeanScan;
import com.tczs.springmvc.annotation.MappingScan;

@BeanScan(scan = "com.tczs.springmvc")
@MappingScan(scan = "com.tczs.springmvc")
public class StartApplication {
}
