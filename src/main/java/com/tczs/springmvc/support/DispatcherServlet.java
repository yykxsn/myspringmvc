package com.tczs.springmvc.support;

import com.alibaba.fastjson.JSON;
import com.tczs.ioc.support.LoadBean;
import com.tczs.ioc.support.StartContainer;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileUpload;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;

public class DispatcherServlet extends HttpServlet {

    public static int i = 0;

    @Override
    public void init() throws ServletException {
        try{
            StartContainer sc = new StartContainer();
            MappingScanPackage mappingScanPackage = MappingScanPackage.getMappingScanPackage();
            LoadRestControllerInfo loadRestControllerInfo = new LoadRestControllerInfo();
            loadRestControllerInfo.load();
            System.out.println("springmvc加载完成");
        }catch (ClassNotFoundException e){
            e.printStackTrace();
            return;
        } catch (IllegalAccessException e) {
            e.printStackTrace();
            return;
        } catch (InvocationTargetException e) {
            e.printStackTrace();
            return;
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            this.doPost(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            this.dispatcherServlet(req,resp);
    }

    public void dispatcherServlet(HttpServletRequest req, HttpServletResponse resp){
    	String servletPath = req.getServletPath();
        HandleInfo handleInfo = LoadRestControllerInfo.handleInfoMap.get(servletPath);
        if(handleInfo == null) {
        	 responseInfo("404 NOT FOUND!", resp);
        }else {
            Object[] paramValues = null;
            if(handleInfo.parameterInfoList.size()>0){
                paramValues = new Object[handleInfo.parameterInfoList.size()];
                Map<String, String[]> parameterMap = getParameterMap(req);
                for (HandleInfo.ParameterInfo parameterInfo : handleInfo.parameterInfoList) {
                    String value = Arrays.toString(parameterMap.get(parameterInfo.getParamName()));
                    paramValues[parameterInfo.getIndex()] = valueConvertType(value, parameterInfo.getParamType());
                }
            }
            Object obj = LoadBean.beanMap.get(handleInfo.controller);
            try {
                Object invoke = handleInfo.method.invoke(obj, paramValues);
                responseInfo(JSON.toJSONString(invoke), resp);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } 
        }
    }

    private void responseInfo(String content,HttpServletResponse resp) {
    	 resp.setContentType("application/json;charset=UTF-8");
         try {
			resp.getWriter().write(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
    	
    }
    
    /**
     * 获取从前端传过来的参数
     * @param req
     * @return
     */
    public Map<String, String[]> getParameterMap(HttpServletRequest req){
        Map<String, String[]> parameterMap = new HashMap<>(); 
    	String contentType = req.getContentType();
         if (contentType != null && contentType.contains("multipart/form-data")) {
        	 String encoding = "ISO-8859-1";
        	 FileItemFactory factory = new DiskFileItemFactory();
             FileUpload fileUpload = new FileUpload(factory);
             fileUpload.setHeaderEncoding(encoding);
             List<FileItem> fileItems = new ArrayList<>();
			try {
				fileItems = fileUpload.parseRequest(req);
			} catch (FileUploadException e) {
				e.printStackTrace();
			}
         	 for(FileItem fileItem : fileItems) {
    			if (fileItem.isFormField()) {
    				String value;
    				try {
    					value = fileItem.getString(encoding);
    				}
    				catch (UnsupportedEncodingException ex) {
    					value = fileItem.getString();
    				}
    				String[] curParam = parameterMap.get(fileItem.getFieldName());
    				if (curParam == null) {
    					parameterMap.put(fileItem.getFieldName(), new String[] {value});
    				}
    				else {
    					String[] newParam = StringUtils.addStringToArray(curParam, value);
    					parameterMap.put(fileItem.getFieldName(), newParam);
    				}
    			}
    		}
         }else {
        	 parameterMap = req.getParameterMap();
         }
    	
     	return parameterMap;
    }
    
    /**
     * 将值转化成对应参数类型的值
     * @param value
     * @param clazz
     * @return
     */
    private <T> T valueConvertType(String value,Class<T> clazz){
    	if(value == null || "null".equals(value)) {
    		return null;
    	}
    	value = value.substring(1,value.length()-1);
    	if(clazz == String.class) {
    		return (T) value;
    	}else if(clazz == int.class || clazz == Integer.class) {
    		return (T) Integer.valueOf(value);
    	}else if(clazz == long.class || clazz == Long.class) {
    		return (T) Long.valueOf(value);
    	}
       return (T) value;
    }
}
