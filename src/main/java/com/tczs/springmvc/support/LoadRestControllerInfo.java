package com.tczs.springmvc.support;

import com.tczs.springmvc.annotation.RequestMapping;
import com.tczs.springmvc.annotation.RestController;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LoadRestControllerInfo {

    public static Map<String,HandleInfo> handleInfoMap = new HashMap<>();

    public void load() throws ClassNotFoundException, InvocationTargetException, IllegalAccessException {
        for(Class clazz : MappingScanPackage.getMappingScanPackage().classList){
            if(clazz.isAnnotationPresent(RestController.class)){
               String interfaceUrl = "";
                Annotation anno = clazz.getAnnotation(RestController.class);
                Method[] annoMethods = anno.annotationType().getMethods();
                for(Method method : annoMethods){
                    if(!method.isAccessible()){
                        method.setAccessible(true);
                    }
                    if("url".equals(method.getName())){
                        interfaceUrl = method.invoke(anno,null).toString();
                        break;
                    }
                }
                loadMethod(clazz,interfaceUrl);
            }
        }
    }

    public void loadMethod(Class clazz,String interfaceUrl) throws InvocationTargetException, IllegalAccessException {
        Method[] contMethods = clazz.getMethods();
        for(Method contMethod : contMethods){
            if(contMethod.isAnnotationPresent(RequestMapping.class)){
                String requestUrl = "";
                Annotation anno = contMethod.getAnnotation(RequestMapping.class);
                Method[] methods = anno.annotationType().getMethods();
                for(Method method : methods){
                    if(!method.isAccessible()){
                        method.setAccessible(true);
                    }
                    if("url".equals(method.getName())) {
                        requestUrl = method.invoke(anno, null).toString();
                        break;
                    }
                }
                HandleInfo handleInfo = new HandleInfo(clazz,contMethod,interfaceUrl+requestUrl);
                handleInfoMap.put(interfaceUrl+requestUrl,handleInfo);
            }
        }
    }
}
