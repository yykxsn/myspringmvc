package com.tczs.springmvc.support;

import com.tczs.ioc.support.BeanScanPackage;
import com.tczs.springmvc.annotation.MappingScan;

import java.io.File;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

public class MappingScanPackage {

    List<String> classPaths = new ArrayList<>();

    List<Class> classList = new ArrayList<>();

    private static MappingScanPackage mappingScanPackage;

    private MappingScanPackage() throws ClassNotFoundException {
        String scanPath = getScanPath();
        searchClass(scanPath);
    }

    public static MappingScanPackage getMappingScanPackage() throws ClassNotFoundException {
        if (mappingScanPackage == null) {
            mappingScanPackage = new MappingScanPackage();
        }
        return mappingScanPackage;
    }


    /**
     * 从StartApplication类的注解MappingScan上得到扫描路径
     * @return
     */
    public String getScanPath() throws ClassNotFoundException {
        BeanScanPackage beanScanPackage = BeanScanPackage.getBeanScanPackage();
        String realClassName = beanScanPackage.convertClassPath(beanScanPackage.startApplicationClassPath,beanScanPackage.allClassPath);
        Class clazz = Class.forName(realClassName);

        if(clazz.isAnnotationPresent(MappingScan.class)){
            MappingScan anno = (MappingScan) clazz.getAnnotation(MappingScan.class);
            Method[] methods = anno.annotationType().getDeclaredMethods();
            for(Method method : methods ){
                if(!method.isAccessible()){
                    method.setAccessible(true);
                }
                try {
                    String scanPath = method.invoke(anno, null).toString();
                    if(scanPath == null || scanPath.length() == 0){
                        throw new RuntimeException("Scan of MappingScan can not is null or \"\"");
                    }
                    return scanPath;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return null;
    }

    /**
     * 查找扫描路径包下的所有Class,放入classList
     * @param
     * @throws ClassNotFoundException
     */
    public void searchClass(String scanPath) throws ClassNotFoundException {
        BeanScanPackage beanScanPackage = BeanScanPackage.getBeanScanPackage();
        scanPath = scanPath.replace(".", File.separator);
        String searchPath = beanScanPackage.allClassPath + scanPath;

        doPath(new File(searchPath));

        for (String classPath : classPaths) {
            String className = beanScanPackage.convertClassPath(classPath,beanScanPackage.allClassPath);
            Class cls = Class.forName(className);
            classList.add(cls);
        }
    }

    private void doPath(File file) {

        if (file.isDirectory()) {
            File[] files = file.listFiles();
            for (File f1 : files) {
                doPath(f1);
            }
        } else {
            if (file.getName().endsWith(".class")) {
                classPaths.add(file.getPath());
            }
        }
    }
}
