package com.tczs.springmvc.support;

public class StringUtils {

	public static String[] addStringToArray(String[] array, String str) {
		if (array == null || array.length == 0) {
			return new String[] {str};
		}

		String[] newArr = new String[array.length + 1];
		System.arraycopy(array, 0, newArr, 0, array.length);
		newArr[array.length] = str;
		return newArr;
	}
	
}
