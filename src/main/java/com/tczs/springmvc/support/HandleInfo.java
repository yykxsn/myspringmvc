package com.tczs.springmvc.support;

import com.tczs.springmvc.annotation.Param;

import java.lang.annotation.Annotation;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Parameter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HandleInfo {

    public Class controller;
    public Method method;
    public String interfaceUrl;
    public List<ParameterInfo> parameterInfoList = new ArrayList<>();

    public HandleInfo(Class controller,Method method,String interfaceUrl){
        this.controller = controller;
        this.method = method;
        this.interfaceUrl = interfaceUrl;
        loadMethodParams(method);
    }

    private void loadMethodParams(Method method){
        Class<?>[] parameterTypes = method.getParameterTypes();
        Annotation[][] parameterAnnotations = method.getParameterAnnotations();
        for(int i=0;i<parameterAnnotations.length;i++) {
            for (int j = 0; j < parameterAnnotations[i].length; j++) {
                Annotation annotation = parameterAnnotations[i][j];
                if (annotation instanceof Param) {
                    Method[] methods = annotation.annotationType().getDeclaredMethods();
                    for (Method me : methods) {
                        if (!me.isAccessible()) {
                            me.setAccessible(true);
                        }
                        if("value".equals(me.getName())){
                            String paramName = "";
                            try {
                                paramName = me.invoke(annotation, null).toString();
                            } catch (IllegalAccessException e) {
                                e.printStackTrace();
                            } catch (InvocationTargetException e) {
                                e.printStackTrace();
                            }
                            ParameterInfo parameterInfo = new ParameterInfo(paramName,parameterTypes[i],i);
                            parameterInfoList.add(parameterInfo);
                        }
                    }
                }
            }
        }
    }

    public Class getController() {
        return controller;
    }

    public Method getMethod() {
        return method;
    }

    public String getInterfaceUrl() {
        return interfaceUrl;
    }

    class ParameterInfo{
        private String paramName;
        private Class paramType;
        private int index;

        public ParameterInfo(String paramName, Class paramType, int index) {
            this.paramName = paramName;
            this.paramType = paramType;
            this.index = index;
        }

        public String getParamName() {
            return paramName;
        }

        public Class getParamType() {
            return paramType;
        }

        public int getIndex() {
            return index;
        }
    }

}
