package com.tczs.springmvc;

import java.util.List;

import com.tczs.ioc.annotation.Autowired;
import com.tczs.ioc.annotation.Component;
import com.tczs.springmvc.annotation.Param;
import com.tczs.springmvc.annotation.RequestMapping;
import com.tczs.springmvc.annotation.RestController;

@Component
@RestController(url="/test")
public class TestController {

    @Autowired
    private TestService testService;

    @RequestMapping(url="/comeIn")
    public void test(){
        System.out.println("comeIn-----进来了!");
    }

    @RequestMapping(url="/findById")
    public TestData.Person findById(@Param("id")Integer id){
        System.out.println("id-----"+id);
        return  testService.findById(Integer.valueOf(id));
    }

    @RequestMapping(url="/findByParam")
    public List<TestData.Person> findByParam(@Param("id")Integer id,@Param("name")String name,@Param("mobile")String mobile){
        System.out.println("name-----"+name);
        return testService.findByParam(id, name, mobile);
    }
}
