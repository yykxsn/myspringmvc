# 手写springmvc框架

## 功能

	* 简单模仿springmvc框架请求分发功能
	* @RestController: 注解在类上,标明这个类是请求映射类
	* @RequestMapping： 注解在方法上,为这个方法添加请求映射地址
	* @Param: 注解在方法参数上,和前端传过来的参数一一映射
	* @MappingScan: 注解在类上,标明扫描哪些包下的@RestController
	
## 使用说明

	* 框架核心代码只是annotation包和support包下的代码,上层目录包下的代码只是我写的测试类
	* 必须创建StartApplication类,在此类上开启@MappingScan注解扫描 
	* 此项目是个web项目,可直接部署在tomcat中启动测试
	
# 此版本已经自动依赖了myioc框架